<?php
function ubah_huruf($string){
//kode di sini
	$huruf = ['a','b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
             'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  $huruf_inputan = [];
  $hasil_inputan = [];

  for ($i=0; $i < strlen($string) ; $i++) {
  	array_push($huruf_inputan, $string[$i]);
  }

  for ($j=0; $j < count($huruf_inputan); $j++) {
    $key = array_search($huruf_inputan[$j], $huruf);
    $key = $key + 1; 
    $hasil = $huruf[$key];
    array_push($hasil_inputan, $hasil);
  }

  $hasil_akhir = implode("",$hasil_inputan);
  return $hasil_akhir;


}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>